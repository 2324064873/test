## CSS 笔记



## 让子类框中的box居中

```css
.parent {
      width: 500px;
      height: 500px;
      background: red;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .child {
      width: 100px;
      height: 100px;
      background: blue;
    }
```

## 居中1

```html
    .parent {
      background-color: pink;
      position: relative;
      height: 200px;
    }

    .child {
      width: 50px;
      height: 50px;
      position: absolute;
      background-color: blue;
      left: 50%;
      top: 50%;

      /* 宽高的一半 */
      margin-top: -25px;
      margin-left: -25px;
    }

  <div class="parent">
    <div class="child"></div>
  </div>
```

## 居中2

```html
    .parent {
      position: relative;
      height: 200px;
      background-color: pink;
    }

    .child {
      width: 50px;
      height: 50px;
      position: absolute;
      background-color: blue;
	
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
    }

  <div class="parent">
    <div class="child"></div>
  </div>
```



## 居中3

```html
	.parent {
      position: relative;
      height: 200px;
      background-color: pink;
    }

    .child {
      width: 50px;
      height: 50px;
      position: absolute;
      background-color: blue;

      left: calc(50% - 50px);
      top: calc(50% - 50px);
    }

  <div class="parent">
    <div class="child"></div>
  </div>
```

## 居中4

```html
    .parent {
      position: relative;
      height: 200px;
      background-color: pink;
    }

    .child {
      width: 50px;
      height: 50px;
      position: absolute;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      background-color: blue;
    }
  <div class="parent">
    <div class="child"></div>
  </div>
```

## 居中5

```html
    .parent {
      padding: 10px;
      background-color: red;
    }

    .child {
      height: 200px;
      background-color: blue;
    }

  <div class="parent">
    <div class="child"></div>
  </div>
```

## 居中6（推荐)

```html
.parent {
      height: 200px;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: red;
    }

    .child {
      width: 50px;
      height: 50px;
      background-color: blue;
    }

  <div class="parent">
    <div class="child"></div>
  </div>
```



## text-align

```html
 //水平居中

	.parent {
      background-color: red;
      text-align: center;
    }

  <div class="parent">
    <span>content</span>
  </div>
```

## fit-content

```html
//水平居中
.parent {
      background-color: red;
      width: fit-content;
      margin: auto;
    }

 <div class="parent">
    <span>content</span>
  </div>
```

## line-height

```html
//垂直居中
    .parent {
      height: 200px;
      line-height: 200px;
      background-color: red;
    }

  <div class="parent">
    <span>content</span>
  </div>
```

## margin: 0 auto

```html
    .parent {
      background-color: pink;
    }

    .child {
      background-color: blue;
      margin: 0 auto; 
      width: 50px;
      height: 50px;
    }

  <div class="parent">
    <div class="child"></div>
  </div>

```



## input: focus

```java
//伪类选择器
input:focus {
      background-color: pink;
    }
```

## CSS的元素显示模式

| 元素模式   | 元素排列           | 设置样式 | 默认宽度         | 包含                     |
| ---------- | ------------------ | -------- | ---------------- | ------------------------ |
| 块级元素   | 一行一个           | 可宽高   | 容器的100%       | 可包含任何标签           |
| 行内元素   | 一行多个           | 不可宽高 | 它本身内容的宽度 | 内容本身或则其他行内元素 |
| 行内块元素 | 一行多个行内块元素 | 可       | 它本身内容的宽度 |                          |

## display:block

```java
//将行内元素 转化为 块级元素
```

## display:inline

```java
//将块级元素转化为行内元素
```

## 权重

| 选择器               | 权重       |
| -------------------- | ---------- |
| 继承 或者 *          | 0，0，0，0 |
| 元素选择器           | 0，0，0，1 |
| 类选择器，伪类选择器 | 0，0，1，0 |
| ID选择器             | 0，1，0，0 |
| 行内样式 style=""    | 1，0，0，0 |
| ！important          | ∞          |

## 文字自动撑大盒子

```css
   /* 不设置宽度 放置初始值padding */
    .box {
      display:inline-block;
      height: 20px;
      padding: 0 10px 0 10px;
      border: 1px solid gray;
    }


    <div class="box">123</div>
    <div class="box">12</div>
    <div class="box">1</div>

```

## margin 外边距合并

[外边距合并](https://www.w3school.com.cn/css/pro_css_margin_collapsing.asp)

## margin塌陷

```css
1. 为父元素定义上边框
2. 为父元素定义上内边距
3. 为父元素添加 overflow:hidden
```

## 浮动

```css
/*标签有了浮动元素后 就已经是行内块元素了*/

   span,div {
      float: left;
      width: 100px;
      height: 100px;
      background-color: pink;
    }

  <span>1</span>
  <div>div</div>
```

## 清除浮动

```css
/*推荐*/
.clearfix:before,.clearfix:after{
  content: "";
  display: table;
}
.clearfix:after{
  clear: both;
}
.cleafix{
  *zoom:1;
}
```

```css
/*最常用*/
.clearfix:after{

      content:"";

      display:block;

      clear:both;

      height:0;

      visibility:hidden;

}
```



```css
/*额外标签法*/
/*清除浮动的本质是清除浮动元素脱离标准流造成的影响*/
/*最后标签添加：*/
    .clear{
      clear:both;
    }

    <div class="num"> 1</div>
    <div class="clear"></div>
```

```css
/*给父级添加 overflow:hidden*/
.box {
      overflow:hidden;
    }

<div class="box">
    <div class="num">1</div>
    <div class="num">1</div>
</div>
```

## CSS属性书写顺序

> 建议遵循以下顺序

 	1. 布局定位属性: display / position / float / clear / visibility / overflow (建议 display 第一个写 毕竟关系到模式)
 	2. 自身属性 : width / height / margin / padding / border / background
 	3. 文本属性 : color / font / text-decoration / text-align / vertical-align / white-space / break-word
 	4. 其他属性 : content / cursor / border-radius / box-shadow / text-shadow / background........

## 导航栏注意点

> 不能直接用链接a 而是用li 包含链接a的做法 
>
> 1.语法清晰
>
> 2.防止降权

